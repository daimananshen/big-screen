import { defineComponent, reactive, onMounted, ref, onUnmounted, watch, nextTick } from 'vue';
import Draw from './draw'

export default defineComponent({
  components: {
    Draw
  },
  setup() {
    const drawTiming = ref(0)

    // methods
    const setData = () => {

    }

    // 定时函数
    const drawTimingFn = () => {
      setData();
    }

    // 生命周期
    onMounted(() => {
      drawTimingFn()
    })

    onUnmounted(() => {
    })

    return () => {
      return <div>
        <Draw  ref="container"  />
      </div>
    }


  }
})