import { defineComponent, watch, ref, onMounted, onUnmounted, reactive } from 'vue';
import { setup } from 'vue-class-component';
import Charts from '@jiaminghi/charts'
// 声明类型
const PropsType = {
  cdata: {
    type: Object,
    require: true
  },
  options: {
    type: Object,
    require: true
  }
} as const



const categories = (function () {
  let now = new Date();
  let res = [];
  let len = 10;
  while (len--) {
    res.unshift(now.toLocaleTimeString().replace(/^\D*/, ''));
    now = new Date(+now - 2000);
  }
  return res;
})();

const categories2 = (function () {
  let res = [];
  let len = 10;
  while (len--) {
    res.push(10 - len - 1);
  }
  return res;
})();

const data: number[] = (function () {
  let res = [];
  let len = 10;
  while (len--) {
    res.push(Math.round(Math.random() * 1000));
  }
  return res;
})();

const data2: number[] = (function () {
  let res = [];
  let len = 0;
  while (len < 10) {
    res.push(+(Math.random() * 10 + 5).toFixed(1));
    len++;
  }
  return res;
})();


// 定义主体
export default defineComponent({
  props: PropsType,
  setup(props) {

    const drawTiming = ref(0)
    const count = ref(11)

    // 定义 ref
    const chartRef = ref()
    // 定义颜色
    const colorList = {
      linearYtoG: {
        type: "linear",
        x: 0,
        y: 0,
        x2: 1,
        y2: 1,
        colorStops: [
          {
            offset: 0,
            color: "#f5b44d"
          },
          {
            offset: 1,
            color: "#28f8de"
          }
        ]
      },
      linearGtoB: {
        type: "linear",
        x: 0,
        y: 0,
        x2: 1,
        y2: 0,
        colorStops: [
          {
            offset: 0,
            color: "#43dfa2"
          },
          {
            offset: 1,
            color: "#28f8de"
          }
        ]
      },
      linearBtoG: {
        type: "linear",
        x: 0,
        y: 0,
        x2: 1,
        y2: 0,
        colorStops: [
          {
            offset: 0,
            color: "#1c98e8"
          },
          {
            offset: 1,
            color: "#28f8de"
          }
        ]
      },
      areaBtoG: {
        type: "linear",
        x: 0,
        y: 0,
        x2: 0,
        y2: 1,
        colorStops: [
          {
            offset: 0,
            color: "rgba(35,184,210,.2)"
          },
          {
            offset: 1,
            color: "rgba(35,184,210,0)"
          }
        ]
      }
    }

    // 配置项
    let options = {}
    // 监听
    watch(
      () => props.cdata,
      (val: any) => {
        options = {
          title: {
            text: ''
          },
          tooltip: {
            trigger: 'axis',
            axisPointer: {
              type: 'cross',
              label: {
                backgroundColor: '#283b56'
              }
            }
          },
          legend: {},
          toolbox: {
            show: true,
            feature: {
              dataView: { readOnly: false },
              restore: {},
              saveAsImage: {}
            }
          },
          dataZoom: {
            show: false,
            start: 0,
            end: 100
          },
          xAxis: [
            {
              type: 'category',
              boundaryGap: true,
              data: categories
            },
            {
              type: 'category',
              boundaryGap: true,
              data: categories2
            }
          ],
          yAxis: [
            {
              type: 'value',
              scale: true,
              name: 'Price',
              max: 30,
              min: 0,
              boundaryGap: [0.2, 0.2]
            },
            {
              type: 'value',
              scale: true,
              name: 'Order',
              max: 1200,
              min: 0,
              boundaryGap: [0.2, 0.2]
            }
          ],
          series: [
            {
              name: 'Dynamic Bar',
              type: 'bar',
              xAxisIndex: 1,
              yAxisIndex: 1,
              data: data
            },
            {
              name: 'Dynamic Line',
              type: 'line',
              data: data2
            }
          ]
        };
        // 手动触发更新
        if (chartRef.value) {
          // 通过初始化参数打入数据
          chartRef.value.initChart(options)
        }
      },
      {
        immediate: true,
        deep: true
      }
    )

    // methods
    const setData = () => {
   

      setInterval(function () {
        // 清空轮询数据

        let axisData = new Date().toLocaleTimeString().replace(/^\D*/, '');
        data.shift();
        data.push(Math.round(Math.random() * 1000));
        data2.shift();
        data2.push(+(Math.random() * 10 + 5).toFixed(1));

        categories.shift();
        categories.push(axisData);
        categories2.shift();
        categories2.push(count.value++);

        const container = document.getElementById("container");
        const myChart = new Charts(container);

        myChart.setOption({
          xAxis: [
            {
              data: categories
            },
            {
              data: categories2
            }
          ],
          series: [
            {
              data: data
            },
            {
              data: data2
            }
          ]
        });
        
        chartRef.value.initChart(options)
       
      }, 1000);


    }

    // 定时函数
    const drawTimingFn = () => {
      setData();
    }

    // 生命周期
    onMounted(() => {
      drawTimingFn()
    })

    onUnmounted(() => {
    })


    return () => {
      const height = "480px"
      const width = "100%"

      return <div>
        <echart ref={chartRef} height={height} width={width} />
      </div>
    }
  }
})

