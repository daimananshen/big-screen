import { defineComponent, watch, ref } from 'vue'
import * as echarts from 'echarts'
// 声明类型
const PropsType = {
  cdata: {
    type: Object,
    require: true
  }
} as const

// 定义主体
export default defineComponent({
  props: PropsType,
  setup(props) {
    // 定义 ref
    const chartRef = ref()
    // 配置项
    let options = {}

    // 监听
    watch(
      () => props.cdata,
      (val: any) => {
        options = {
          tooltip: {
            show: true,
            trigger: "item",
            axisPointer: {
              type: "shadow",
              label: {
                show: true,
              }
            }
          },
          legend: {
            show: true,
          },
          grid: {
            x: "8%",
            width: "85%",
            top: "5%",
            bottom: '7%'
          },
          xAxis: {
            data: val.category,
            axisLine: {
              lineStyle: {
              }
            },
            axisTick: {
              show: false
            }
          },
          yAxis: [
            {
              splitLine: { show: false },
              axisLine: {
                lineStyle: {
                }
              },

              axisLabel: {
                formatter: "{value} "
              }
            },
            {
              splitLine: { show: false },
              axisLine: {
                lineStyle: {
                }
              },
              axisLabel: {
                formatter: "{value} "
              }
            }
          ],
          series: [
            {
              name: "总建筑面积(万米)",
              type: "bar",
              barWidth: 10,
              itemStyle: {
                normal: {
                  barBorderRadius: 5,
                  color: "#00CCFF",
                }
              },
              data: val.lineData
            },
            {
              name: "总金额(亿元)",
              type: "bar",
              barWidth: 10,
              itemStyle: {
                normal: {
                  barBorderRadius: 5,
                  color: "#FFFF00",
                }
              },
              data: val.barData
            },
            {
              name: "平均楼板价(元/m)",
              type: 'line',
              yAxisIndex: 1,
              itemStyle: {
                normal: {
                  color: "#00FF66",
                }
              },
              data: val.rateData
            },
          ]
        }
        // 手动触发更新
        if (chartRef.value) {
          // 通过初始化参数打入数据
          chartRef.value.initChart(options)
        }
      },
      {
        immediate: true,
        deep: true
      }
    )

    return () => {
      const height = "450px"
      const width = "100%"

      return <div>
        <echart ref={chartRef} height={height} width={width} />
      </div>
    }
  }
})

