import { defineComponent, reactive } from 'vue'
import Draw from './draw'

export default defineComponent({
  components: {
    Draw
  },
  setup() {
    const cdata = reactive([
      {
        // 名字需要与 “common/map/guangzhou.js” 地图数据文件里面定义的一一对应
        name: '广州',
        value: 10,
        elseData: {
          // 这里放置地图 tooltip 里想显示的数据
        }
      },
      {
        name: '黄埔',
        value: 8,
      },
      {
        name: '越秀',
        value: 6,
      },
      {
        name: '海珠',
        value: 5,
      },
      {
        name: '白云',
        value: 4,
      },
      {
        name: '荔湾',
        value: 3,
      },
      {
        name: '天河',
        value: 2,
      }
    ])

    return () => {
      return <div>
        <Draw cdata={cdata} />
      </div>
    }
  }
})