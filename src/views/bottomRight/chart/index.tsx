import { defineComponent, reactive, onMounted, ref, onUnmounted } from 'vue';
import Draw from './draw'

export default defineComponent({
  components: {
    Draw
  },
  setup() {
    const drawTiming = ref(0)
    const cdata = reactive({
      weekCategory: [],
      radarDataAvg: [],
      weekLineData: []
    })

    // methods
    const setData = () => {
      // 清空轮询数据
      cdata.weekCategory = [];
      cdata.weekLineData = [];
      cdata.radarDataAvg = [];

      // 13天数据
      cdata.weekCategory = [
        "2019-01",
        "2019-02",
        "2019-03",
        "2019-04",
        "2019-05",
        "2019-06",
        "2019-07",
        "2019-08",
        "2019-09",
        "2019-10",
        "2019-11",
        "2019-12",
        "2020-01"
      ]

      cdata.radarDataAvg =[150, 230, 224, 218, 135, 150, 260, 150, 230, 124, 218, 235, 147]
      cdata.weekLineData = [150, 250, 224, 218, 135, 147, 250, 160, 250, 124, 218, 245, 160]
    }

    // 定时函数
    const drawTimingFn = () => {
      setData();
    }

    // 生命周期
    onMounted(() => {
      drawTimingFn()
    })

    onUnmounted(() => {
    })

    return () => {
      return <div>
        <Draw cdata={cdata} />
      </div>
    }
  }
})