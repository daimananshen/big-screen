import { defineComponent, watch, ref } from 'vue';
// 声明类型
const PropsType = {
  cdata: {
    type: Object,
    require: true
  }
} as const

// 定义主体
export default defineComponent({
  props: PropsType,
  setup(props) {
    // 定义 ref
    const chartRef = ref()
    // 定义颜色
    const colorList = {
      linearYtoG: {
        type: "linear",
        x: 0,
        y: 0,
        x2: 1,
        y2: 1,
        colorStops: [
          {
            offset: 0,
            color: "#f5b44d"
          },
          {
            offset: 1,
            color: "#28f8de"
          }
        ]
      },
      linearGtoB: {
        type: "linear",
        x: 0,
        y: 0,
        x2: 1,
        y2: 0,
        colorStops: [
          {
            offset: 0,
            color: "#43dfa2"
          },
          {
            offset: 1,
            color: "#28f8de"
          }
        ]
      },
      linearBtoG: {
        type: "linear",
        x: 0,
        y: 0,
        x2: 1,
        y2: 0,
        colorStops: [
          {
            offset: 0,
            color: "#1c98e8"
          },
          {
            offset: 1,
            color: "#28f8de"
          }
        ]
      },
      areaBtoG: {
        type: "linear",
        x: 0,
        y: 0,
        x2: 0,
        y2: 1,
        colorStops: [
          {
            offset: 0,
            color: "rgba(35,184,210,.2)"
          },
          {
            offset: 1,
            color: "rgba(35,184,210,0)"
          }
        ]
      }
    }
    // 配置项
    let options = {}

    // 监听
    watch(
      () => props.cdata,
      (val: any) => {
        options = {
          title: {
            text: "",
            textStyle: {
              color: "#D3D6DD",
              fontSize: 24,
              fontWeight: "normal"
            },
            subtextStyle: {
              color: "#fff",
              fontSize: 16
            },
            top: 50,
            left: 80
          },
          legend: {
            top: 30,
            left: 80,
            itemGap: 15,
            itemWidth: 12,
            itemHeight: 12,
            data: ["溢价率", "流拍率"],
            textStyle: {
              color: "#fff",
              fontSize: 14
            }
          },
          tooltip: {
            trigger: "item"
          },
          grid: {
            left: 90,
            right: 80,
            bottom: '15%',
            top: "20%"
          },
          xAxis: {
            type: "category",
            position: "bottom",
            axisLine: true,
            axisLabel: {
              color: "rgba(255,255,255,.8)",
              fontSize: 12
            },
            data: val.weekCategory
          },
          // 下方Y轴
          yAxis: {
            name: "",
            nameLocation: "end",
            nameGap: 24,
            nameTextStyle: {
              color: "rgba(255,255,255,.5)",
              fontSize: 14
            },
            splitNumber: 4,
            axisLine: {
              lineStyle: {
                opacity: 0
              }
            },
            splitLine: {
              show: true,
              lineStyle: {
                color: "#fff",
                opacity: 0.1
              }
            },
            axisLabel: {
              color: "rgba(255,255,255,.8)",
              fontSize: 12,
              formatter: function (value) {
                var texts = [];
                if(value ==50){
                  texts.push('5%');
                }else if (value ==100){
                  texts.push('10%');
                }else if (value ==150){
                  texts.push('15%');
                }else if (value ==200){
                  texts.push('20%');
                }else if (value ==250){
                  texts.push('25%');
                }else if (value ==300){
                  texts.push('30%');
                }
                return texts;
              }
            }
          },
          series: [
            {
              data: val.radarDataAvg,
              name: "溢价率",
              type: 'line'
            },
            {
              data: val.weekLineData,
              name: "流拍率",
              type: 'line'
            }
          ]
        };
        // 手动触发更新
        if (chartRef.value) {
          // 通过初始化参数打入数据
          chartRef.value.initChart(options)
        }
      },
      {
        immediate: true,
        deep: true
      }
    )

    return () => {
      const height = "480px"
      const width = "100%"

      return <div>
        <echart ref={chartRef} height={height} width={width} />
      </div>
    }
  }
})

