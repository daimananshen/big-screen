## 介绍
可视化大屏
大屏数据库组件DataV地址：http://datav.jiaminghi.com/

## 软件架构
vue3 + DataV

## 项目安装

1、先安装 nodejs yarn
2、yarn下载依赖包
3、npm i -g @vue/cli
4、npm run serve


## 目录介绍

文件                            // 作用/功能                                                              
| main.ts                       // 主目录文件，引入注册 自定义组件、DataV 、样式等数据              
| views/*                       // 界面各个区域组件按照位置来命名，index 是项目主结构                   
| constant/*                    // 静态数据项，所有的标题和图标都配置在这里                                
| utils/*                       // 全局公共函数（包含屏幕适配函数）                                          
| assets/*                      // 静态资源目录，放置图片与全局样式（index 文件样式单独放在这里）   
| components/echart             // 封装的全局图表渲染函数                                       
| components/componentInstall   // 全局组件注册位置                                    
| common/*                      // 通用数据配置项（放置 echart 样式与地图数据）                             
| router/*                      // 路由配置区域                                                           
| store/*                       // Vuex 相关区域                                                           
| src/ *.d.ts                   // 全局类型声明文件                                                     

